// SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>
//
// SPDX-License-Identifier: CC-BY-SA-4.0

/*File Info--------------------------------------------------------------------
File Name: SlotBeam.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


length=570;
width=22;
height=24;
notch_l=510;
notch_w=7;
notch_h=24;
hole_d=8;


//projection()
difference()
{
    cube([length,width,height]);
    translate([(length-notch_l)/2,8,height-notch_h])
    cube([notch_l,notch_w,notch_h]);
    translate([20,8+notch_w/2,height/2])
    cylinder(h=height,d=hole_d,center=true);
    translate([length-20,8+notch_w/2,height/2])
    cylinder(h=height,d=hole_d,center=true); 
}