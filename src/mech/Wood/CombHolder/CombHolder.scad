// SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>
//
// SPDX-License-Identifier: CC-BY-SA-4.0

/*File Info--------------------------------------------------------------------
File Name: CombHolder.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


thickness=18;
corner=thickness/6;
hole=6;
drillhole=7;

//projection()
difference()
{
    union()
    {
        hull()
        {
            translate([corner/2,corner/2,thickness/2])
            cylinder(h=thickness,d=corner,center=true);
            translate([thickness/2,44-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([40-corner/2,corner/2,thickness/2])
            cylinder(h=thickness,d=corner,center=true);
            translate([40-thickness/2,44-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
        }
        translate([40,25+38,thickness/2])
        cube([40,70,thickness],center=true);
        hull()
        {
            translate([20+corner/2,118-corner/2,thickness/2])
            cylinder(h=thickness,d=corner,center=true);
            translate([20+thickness/2,94-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([40-corner/2,118-corner/2,thickness/2])
            cylinder(h=thickness,d=corner,center=true);
            translate([40-thickness/2,94-thickness,thickness/2])
            cube([thickness,thickness,thickness],center=true);
        }
    }
    translate([20,20,thickness/2])
    cylinder(h=thickness,d=drillhole,center=true);
    translate([40,68,thickness/2])
    cylinder(h=thickness,d=drillhole,center=true);
    translate([18,46,thickness/2])
    cylinder(h=thickness,d=hole,center=true);
    translate([42,100,thickness/2])
    cylinder(h=thickness,d=hole,center=true);
    translate([42,26,thickness/2])
    cylinder(h=thickness,d=hole,center=true);
}