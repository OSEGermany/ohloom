// SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>
//
// SPDX-License-Identifier: CC-BY-SA-4.0

/*File Info--------------------------------------------------------------------
File Name: SideFrame.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------

thickness=18;
corner1=36;
corner2=thickness;
cornercut=20;
hole1=37;
hole2=10;
hole3=7;


//projection()
difference()
{
    union()
    {
        hull()
        {
            translate([0+(corner1/2),corner1,thickness/2])
            cylinder(h=thickness,d=corner1,center=true);
            translate([0+(corner1/2),140-corner1/2,thickness/2])
            cylinder(h=thickness,d=corner1,center=true);
            translate([75-thickness/2,140-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([125-thickness/2,60-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([125-thickness/2,thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
        }
        hull()
        {
            translate([312+thickness/2,thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([372-thickness/2,thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([312+corner2/2,140-corner2/2,thickness/2])
            cylinder(h=thickness,d=corner2,center=true);
            translate([372-corner2/2,140-corner2/2,thickness/2])
            cylinder(h=thickness,d=corner2,center=true);
        }
        hull()
        {
            translate([455+thickness/2,60-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([455+thickness/2,thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([505+thickness/2,140-thickness/2,thickness/2])
            cube([thickness,thickness,thickness],center=true);
            translate([580-corner1/2,140-corner1/2,thickness/2])
            cylinder(h=thickness,d=corner1,center=true);
            translate([580-corner1/2,cornercut+17,thickness/2])
            cylinder(h=thickness,d=corner1,center=true);
        }
        translate([280,30,thickness/2])
        cube([530,cornercut*3,thickness],center=true);
    }
    //Drill holes
    //Big
    translate([50,110,thickness/2])
    cylinder(h=thickness,d=hole1,center=true);
    translate([530,110,thickness/2])
    cylinder(h=thickness,d=hole1,center=true);
    //Small
    translate([120,10,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([150,10,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([430,10,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([460,10,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([352,30,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([332,78,thickness/2])
    cylinder(h=thickness,d=hole3,center=true);
    translate([550,cornercut/2,thickness/2])
    cube([80,cornercut,thickness],center=true);
    translate([hole3,cornercut/2,thickness/2])
    rotate([0,0,-20])
    cube([120,cornercut,thickness],center=true);
}

