# OHLOOM - User Guide \[ENG\]

<!--
SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[**\[GER\]**](index_de.md)

![](../../res/assets/media/img/User_Guide.jpg)

## Index

1. [Prepare Wind Up](#prep-windup)
2. [Wind Up Utilities](#windup-utilities)
3. [Wind It Up](#wind-it-up)
4. [How to weave with OHLOOM](#how-to-weave)

## 1. Prepare Wind Up {#prep-windup}

To protect the whool while winding up, you need to add carton stripes onto the beam.
2 cm width and 55-57 cm long.

| | | |
| :---------: | :------------: | :---: |
| ![](../../res/assets/media/img/PrepWindUp_1.jpg) 1. Draw lines | ![](../../res/assets/media/img/PrepWindUp_2.jpg) 2. Use a cutting knife | ![](../../res/assets/media/img/PrepWindUp_3.jpg) 3. 10-20 stripes |

## 2. Wind Up Utilities {#windup-utilities}

| | |
| :---------: | :------------: |
| ![](../../res/assets/media/img/WindUp_1.jpg) 2 x Big Clamps, 1 Small Clamp | ![](../../res/assets/media/img/WindUp_2.jpg) 1 x Scissor |
| ![](../../res/assets/media/img/WindUp_3.jpg) 1 X Hook | ![](../../res/assets/media/img/WindUp_4.jpg) 1 x Pole |

## 3. Wind It Up {#wind-it-up}

| |
| :---------: |
| 1. Measure the distance of your project (e.g. length of scarf) |
| 2. Clamp the pol on one side ![](../../res/assets/media/img/WindUp_4.jpg) |
| 3. Clamp the loom on the other side! Be aware to place the short end of the loom towards the outside! ![](../../res/assets/media/img/WindUp_5.jpg) |
| 4. Leave whool on the floor and lead the end up between the warpclothbeam and crossbeam and knot it onto the stringstick ![](../../res/assets/media/img/WindUp_6.jpg) |
| 5. Now lead it between the long slits of the combmodule and around the pol ![](../../res/assets/media/img/WindUp_7.jpg) |
| 6. Take the next sling and lead it over the stringstick ![](../../res/assets/media/img/WindUp_8.jpg) |
| 7. Continue with all long slits, one over, one under the stringstick ![](../../res/assets/media/img/WindUp_9.jpg) |
| 8. Lead it each time around the pol ![](../../res/assets/media/img/WindUp_10.jpg) |
| 9. If you want change the color, just cut it, make a knot around the stringstick, and continue ![](../../res/assets/media/img/WindUp_19.jpg) ![](../../res/assets/media/img/WindUp_20.jpg) ![](../../res/assets/media/img/WindUp_21.jpg) |
| 10. After you finished with all slits, use some whool to bind all strings to a bundle just behind the pol ![](../../res/assets/media/img/WindUp_22.jpg) ![](../../res/assets/media/img/WindUp_23.jpg) |
| 11. Look for someone to help you holding the bundle end to keep some tension |
| 12. Position the comb in the idle position at the other end of the combholder ![](../../res/assets/media/img/WindUp_24.jpg) |
| 13. Start rolling in the whool, while putting in the prepared carton stripes, till you can not roll it in any further ![](../../res/assets/media/img/WindUp_25.jpg) ![](../../res/assets/media/img/WindUp_26.jpg) |
| 14. Use the scissor to cut the slings, and open the knot around the bundle ![](../../res/assets/media/img/WindUp_27.jpg) |
| 15. Now you can take of the clamps, and turn around the loom ![](../../res/assets/media/img/WindUp_28.jpg) |
| 16. Go through every slot, take one of the 2 strings, and use the hook to lead it through the small slot aside each long slot ![](../../res/assets/media/img/WindUp_29.jpg) |
| 17. Bundle around 10 strings with a knot ![](../../res/assets/media/img/WindUp_30.jpg) |
| 18. Use a string (ca. 2 m), half it by leading it around the stringstick ![](../../res/assets/media/img/WindUp_31.jpg) |
| 19. Now wind it around the stringstick and through each bundle |
| 20. Keep it tight, and make a knot at the end. The whole strings should be straight and tight ![](../../res/assets/media/img/WindUp_32.jpg) |
| 21. Take the shuttle, and the whole and proceed as seen on the images ![](../../res/assets/media/img/WindUp_33.jpg) ![](../../res/assets/media/img/WindUp_34.jpg) |
| 22. Move the comb out of idle position into the upper or lower position to be able to do the next step |
| 23. Again, take some whool and double it. Use the shuttle to lead it through the strings. |
| 24. Move the comb to the opposing position (up or down), and lead the doubled whool strings through ![](../../res/assets/media/img/WindUp_35.jpg) |
| 23. Repeat it several times, then move the comb slowly towards you to tighten the doubled whool strings! You will recognize, that the distances between the strings got nearly equalized! ![](../../res/assets/media/img/WindUp_36.jpg) |

### Congrats! Now the loom is ready for weaving!

## 4. How to weave with OHLOOM {#how-to-weave}

| |
| :---------: |
| 1. Position comb up and shuttle through ![](../../res/assets/media/img/WindUp_37.jpg) |
| 2. Hold the thread on one side and position it in a diagonal matter ![](../../res/assets/media/img/WindUp_38.jpg) |
| 3. Move the string with the comb, and push it a several times ![](../../res/assets/media/img/WindUp_39.jpg) |
| 4. Position down and shuttle through ![](../../res/assets/media/img/WindUp_40.jpg) |
| 5. Move the threat using the comb and continue with 1. ![](../../res/assets/media/img/WindUp_41.jpg) |

### And so on and forth ... Good luck and have fun with your projects!
