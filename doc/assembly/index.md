# OHLOOM - Assembly Guide \[ENG\]

<!--
SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[**\[GER\]**](index_de.md)

![](../../res/assets/media/img/Assembly_Guide.jpg)

## Index

1. Assembly Parts
   1. [Metal Parts](#metal-parts)
   2. [3D Print Parts](#print-parts)
   3. [CNC Parts](#cnc-parts)
   4. [Other Parts](#other-parts)
2. Assembly Preparation
   1. [Colorful Wood Protection Film](#protection)
3. [Assembly](#assembly)

## 1. Assembly Parts

### 1.1. Metal Parts {#metal-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_Metal_1.jpg) 2 x Threaded rod M8 x 180 mm |  ![](../../res/assets/media/img/Parts_Metal_2.jpg) 4 x M8 Nuts |  ![](../../res/assets/media/img/Parts_Metal_3.jpg) 2 x M8 Nuts with Rubber |
| ![](../../res/assets/media/img/Parts_Metal_4.jpg) 4 x M6 Nuts |  ![](../../res/assets/media/img/Parts_Metal_5.jpg) 4 x M6 Cylinder Screw |
| ![](../../res/assets/media/img/Parts_Metal_6.jpg) 8 x W60x4 Wood Screw |  ![](../../res/assets/media/img/Parts_Metal_7.jpg) 4 x W35x4 Wood Screw |  ![](../../res/assets/media/img/Parts_Metal_8.jpg) 16 x W12x4 Wood Screw |

### 1.2. 3D Print Parts {#print-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_Print_1.jpg) 2 x Ratchet Wheel |  ![](../../res/assets/media/img/Parts_Print_2.jpg) 2 x Clamp Ring |  ![](../../res/assets/media/img/Parts_Print_3.jpg) 5 x Comb Module |
| ![](../../res/assets/media/img/Parts_Print_4.jpg) 2 x Ratchet Pawl |  ![](../../res/assets/media/img/Parts_Print_5.jpg) 12 x Screw Socket w |  ![](../../res/assets/media/img/Parts_Print_6.jpg) 4 x Screw Socket w/o |
| ![](../../res/assets/media/img/Parts_Print_7.jpg) 2 x Outer Warpcloth Beam (4 parts each) |

### 1.3. CNC Parts (Wood) {#cnc-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_CNC_1.jpg) 2 x Comb Holder |  ![](../../res/assets/media/img/Parts_CNC_2.jpg) 2 x Side Frame |  ![](../../res/assets/media/img/Parts_CNC_3.jpg) 2 x Cross Beam |
| ![](../../res/assets/media/img/Parts_CNC_4.jpg) 2 x Slot Beam |  ![](../../res/assets/media/img/Parts_CNC_5.jpg) 2 x String Stick |  ![](../../res/assets/media/img/Parts_CNC_6.jpg) 1 x Shuttle |

### 1.4. Other Parts {#other-parts}

| | |
| :----: | :----: |
| ![](../../res/assets/media/img/Parts_Other_1.jpg) 2 x Inner Warpcloth Beam |  ![](../../res/assets/media/img/Parts_Other_2.jpg) ca. 3 m String |

## 2. Assembly Preparation

### 2.1. Colorful Wood Protection Film {#protection}

| |
| :----: |
| ![](../../res/assets/media/img/Assembly_Preparation_1.jpg) |
| 1. You will need color pigments, line oil, an empty glass, and a Brush  ![](../../res/assets/media/img/Assembly_Preparation_2.jpg) |
| 2. First, fill in the pigments (ca. 10-20 grams).  ![](../../res/assets/media/img/Assembly_Preparation_3.jpg) |
| 3. Next poor 8-10 times as much oil as pigments into the glass.  ![](../../res/assets/media/img/Assembly_Preparation_4.jpg) |
| 4. Stir it well, until you have got a smooth equally colored liquid without spots.  ![](../../res/assets/media/img/Assembly_Preparation_5.jpg) |
| 5. Make sure the wooden surface is clean and dry. |
| 6. Apply 1-3 layer, till you have got the wanted look. |
| 7. Let it dry for several hours (or better, leave it overnight). |

## 3. Assembly {#assembly}

| | | | |
|-|----|----|----|
| 1.  | ![](../../res/assets/media/img/Assembly_1.jpg)  | ![](../../res/assets/media/img/Assembly_2.jpg)  |
| 2.  | ![](../../res/assets/media/img/Assembly_3.jpg)  | ![](../../res/assets/media/img/Assembly_4.jpg)  |
| 3.  | ![](../../res/assets/media/img/Assembly_5.jpg)  | ![](../../res/assets/media/img/Assembly_6.jpg)  |
| 4.  | ![](../../res/assets/media/img/Assembly_7.jpg)  | ![](../../res/assets/media/img/Assembly_8.jpg)  |
| 5.  | ![](../../res/assets/media/img/Assembly_9.jpg)  | ![](../../res/assets/media/img/Assembly_10.jpg) |
| 6.  | ![](../../res/assets/media/img/Assembly_11.jpg) | ![](../../res/assets/media/img/Assembly_12.jpg) |
| 7.  | ![](../../res/assets/media/img/Assembly_13.jpg) | ![](../../res/assets/media/img/Assembly_14.jpg) | ![](../../res/assets/media/img/Assembly_15.jpg) |
| 8.  | ![](../../res/assets/media/img/Assembly_16.jpg) | ![](../../res/assets/media/img/Assembly_17.jpg) | ![](../../res/assets/media/img/Assembly_18.jpg) |
| 9.  | ![](../../res/assets/media/img/Assembly_19.jpg) | ![](../../res/assets/media/img/Assembly_20.jpg) | ![](../../res/assets/media/img/Assembly_21.jpg) |
| 10. | ![](../../res/assets/media/img/Assembly_22.jpg) | ![](../../res/assets/media/img/Assembly_23.jpg) |
| 11. | ![](../../res/assets/media/img/Assembly_24.jpg) |
| 12. | ![](../../res/assets/media/img/Assembly_25.jpg) |

Voila! You just finished assembling your first OHLOOM. Congrats!
