# OHLOOM - Montageanleitung \[GER\]

<!--
SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[**\[ENG\]**](index.md)

![](../../res/assets/media/img/Assembly_Guide.jpg)

## Index

1. Montageteile
   1. [Metallteile](#metal-parts)
   2. [3D Druckteile](#print-parts)
   3. [CNC Teile](#cnc-parts)
   4. [Andere Teile](#other-parts)
2. Montagevorbereitung
   1. [Farbiger Schutzfilm](#protection)
3. [Montage](#assembly)

## 1. Montageteile

### 1.1. Metallteile {#metal-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_Metal_1.jpg) 2 x Gewindebolzen M8 x 180 mm |  ![](../../res/assets/media/img/Parts_Metal_2.jpg) 4 x M8 Muttern |  ![](../../res/assets/media/img/Parts_Metal_3.jpg) 2 x M8 Muttern mit Gummiring |
| ![](../../res/assets/media/img/Parts_Metal_4.jpg) 4 x M6 Muttern |  ![](../../res/assets/media/img/Parts_Metal_5.jpg) 4 x M6 Zylinderschrauben |
| ![](../../res/assets/media/img/Parts_Metal_6.jpg) 8 x W60x4 Holzschrauben |  ![](../../res/assets/media/img/Parts_Metal_7.jpg) 4 x W35x4 Holzschrauben |  ![](../../res/assets/media/img/Parts_Metal_8.jpg) 16 x W12x4 Holzschrauben |

### 1.2. 3D Druckteile {#print-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_Print_1.jpg) 2 x Sperrrad |  ![](../../res/assets/media/img/Parts_Print_2.jpg) 2 x Klemmenring |  ![](../../res/assets/media/img/Parts_Print_3.jpg) 5 x Kammmodul |
| ![](../../res/assets/media/img/Parts_Print_4.jpg) 2 x Sperrkeil |  ![](../../res/assets/media/img/Parts_Print_5.jpg) 12 x Schraubenfuss mit |  ![](../../res/assets/media/img/Parts_Print_6.jpg) 4 x Schraubenfuss ohne |
| ![](../../res/assets/media/img/Parts_Print_7.jpg) 2 Achtkantwellenelemente (4 pro Einheit) |

### 1.3. CNC Teile (Holz) {#cnc-parts}

| | | |
| :----: | :----: | :----: |
| ![](../../res/assets/media/img/Parts_CNC_1.jpg) 2 x Kammhalter |  ![](../../res/assets/media/img/Parts_CNC_2.jpg) 2 x Seitenteil |  ![](../../res/assets/media/img/Parts_CNC_3.jpg) 2 x Querteil |
| ![](../../res/assets/media/img/Parts_CNC_4.jpg) 2 x Einschnittleiste |  ![](../../res/assets/media/img/Parts_CNC_5.jpg) 2 x Schnurleiste |  ![](../../res/assets/media/img/Parts_CNC_6.jpg) 1 x Schiff |

### 1.4. Andere Teile {#other-parts}

| | |
| :----: | :----: |
| ![](../../res/assets/media/img/Parts_Other_1.jpg) 2 x Holzwelle |  ![](../../res/assets/media/img/Parts_Other_2.jpg) ca. 3 m Schnur |

## 2. Montagevorbereitung

### 2.1. Farbiger Schutzfilm {#protection}

| |
| :----: |
| ![](../../res/assets/media/img/Assembly_Preparation_1.jpg) |
| 1. Man braucht Farbpigmente, Leinöl, ein leeres Glass und Pinsel  ![](../../res/assets/media/img/Assembly_Preparation_2.jpg) |
| 2. Zuerst fülle die Pigmente ins Glas (ca. 10-20 Gramm).  ![](../../res/assets/media/img/Assembly_Preparation_3.jpg) |
| 3. Fülle als nächstes 8-10 mal soviel Öl wie Pigmente ins Glass und rühre kräftig.  ![](../../res/assets/media/img/Assembly_Preparation_4.jpg) |
| 4. Rühr solang die Pigmente bis sie gut vermischt sind und keine Klümpchen mehr aufweisen.  ![](../../res/assets/media/img/Assembly_Preparation_5.jpg) |
| 5. Die Holzoberfläche sollte sauber und trocken sein. |
| 6. Trage die Farbe in 1-3  Schichten auf, bis es das gewünschte  Aussehen aufweist. |
| 7. Lass es über Nacht trocknen. |

## 3. Montage {#assembly}

| | | | |
|-|----|----|----|
| 1.  | ![](../../res/assets/media/img/Assembly_1.jpg)  | ![](../../res/assets/media/img/Assembly_2.jpg)  |
| 2.  | ![](../../res/assets/media/img/Assembly_3.jpg)  | ![](../../res/assets/media/img/Assembly_4.jpg)  |
| 3.  | ![](../../res/assets/media/img/Assembly_5.jpg)  | ![](../../res/assets/media/img/Assembly_6.jpg)  |
| 4.  | ![](../../res/assets/media/img/Assembly_7.jpg)  | ![](../../res/assets/media/img/Assembly_8.jpg)  |
| 5.  | ![](../../res/assets/media/img/Assembly_9.jpg)  | ![](../../res/assets/media/img/Assembly_10.jpg) |
| 6.  | ![](../../res/assets/media/img/Assembly_11.jpg) | ![](../../res/assets/media/img/Assembly_12.jpg) |
| 7.  | ![](../../res/assets/media/img/Assembly_13.jpg) | ![](../../res/assets/media/img/Assembly_14.jpg) | ![](../../res/assets/media/img/Assembly_15.jpg) |
| 8.  | ![](../../res/assets/media/img/Assembly_16.jpg) | ![](../../res/assets/media/img/Assembly_17.jpg) | ![](../../res/assets/media/img/Assembly_18.jpg) |
| 9.  | ![](../../res/assets/media/img/Assembly_19.jpg) | ![](../../res/assets/media/img/Assembly_20.jpg) | ![](../../res/assets/media/img/Assembly_21.jpg) |
| 10. | ![](../../res/assets/media/img/Assembly_22.jpg) | ![](../../res/assets/media/img/Assembly_23.jpg) |
| 11. | ![](../../res/assets/media/img/Assembly_24.jpg) |
| 12. | ![](../../res/assets/media/img/Assembly_25.jpg) |

Voila! Dein erster OHLOOM Webstuhl ist zusammengebaut. Glückwunsch!
