<!--
SPDX-FileCopyrightText: 2021 Jens Meisner <jens.meisner@ose-germany.de>
SPDX-FileCopyrightText: 2021-2023 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# OHLOOM

[![License: CC-BY-SA-4.0](
    https://img.shields.io/badge/License-CC%20BY--SA%204.0-blue.svg)](
    https://creativecommons.org/licenses/by-sa/4.0/)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/ohloom)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/ohloom)
[![made-with-Markdown](
    https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](
    https://commonmark.org)
[![made-with-OpenSCAD](
    https://img.shields.io/badge/Made%20with-OpenSCAD-orange.svg)](
    http://openscad.org/)
[![OSH Openness](
    https://osegermany.gitlab.io/ohloom/osh-badge-openness.svg)](
    https://osegermany.gitlab.io/ohloom/osh-report.html)

[![In cooperation with Open Source Ecology Germany](
    https://raw.githubusercontent.com/osegermany/tiny-files/master/res/media/img/badge-oseg.svg)](
    https://opensourceecology.de)

**O**pen **H**ardware **Loom**
(GER: Webstuhl)

You may want to see [the original project Wiki](
https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM),
though this repo practically makes the Wiki version obsolete,
as it contains improvements,
and further development will happen here.

## Guides

| [User Guide][UserGuideEn] | [Assembly Guide][AssemblyGuideEn] |
| :----: | :----: |
| [![][UserGuideImg]][UserGuideEn] | [![][AssemblyGuideImg]][AssemblyGuideEn] |
| [ENG][UserGuideEn] / [GER][UserGuideDe] | [ENG][AssemblyGuideEn] / [GER][AssemblyGuideDe] |

[UserGuideImg]: res/assets/media/img/User_Guide.jpg
[UserGuideEn]: doc/user/index.md
[UserGuideDe]: doc/user/index_de.md
[AssemblyGuideImg]: res/assets/media/img/Assembly_Guide.jpg
[AssemblyGuideEn]: doc/assembly/index.md
[AssemblyGuideDe]: doc/assembly/index_de.md

## Plans for the Parts

The 3D plans for the parts of the loom
are available as source files in the OpenSCAD format.
Alongside you will find read-only versions as generated STL files
and sometimes technical drawings as PDFs.

You will find all these in this repository in the folders
[src/mech/3DPrinted/](src/mech/3DPrinted/) and
[src/mech/Wood/](src/mech/Wood/).
